#!/usr/bin/Rscript

## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
## Utility functions for working with AngioABM and its output, mostly plotting and
## formatting.
##
## Author: Clemens Kuehn
## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

require(ggplot2)
require(data.table)
require(RColorBrewer)
require(fields)
require(viridis)
require(xml2)

## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
## read a model in xml
abm_readxml <- function(filename){
    if(file.exists(filename)){
        xmlfile <- read_xml(filename)
    }else{
        cat(paste0("file ", filename, " does not exist."))
        quit(save = "no", status = 1)
    }
    return(xmlfile)
}

## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
## modify a value in an xml model, identified by a path and a parameter name
abm_modify_path <- function(xml, path, parname, value){
    fullpath <- paste0(path,"/",parname)

    par <- xml_find_all(xml, fullpath)
    xml_text(par) <- as.character(value)

}

## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
## modify a value in an xml model, identified by an entity and a parameter name
abm_modify_entity <- function(xml, entityname, parname, value){

    entity.xml <- xml_find_all(xml, paste0(".//name[text()[.='",entityname,"']]"))
    entity.family <- xml_parent(xml_siblings(entity.xml))
    par <- xml_find_all(entity.family, paste0(".//",parname))

    xml_text(par) <- value
}


## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
## modify a value in an xml model, identified by an entity and a parameter name
abm_get_entity <- function(xml, entityname, parname){

    entity.xml <- xml_find_all(xml, paste0(".//name[text()[.='",entityname,"']]"))
    entity.family <- xml_parent(xml_siblings(entity.xml))
    par <- xml_find_all(entity.family, paste0(".//",parname))

    return(xml_text(par))
}


## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
## get a value in an xml model, identified by a path and a parameter name
abm_get_entity_at_path <- function(xml, path, parname){
    fullpath <- paste0(path,"/",parname)

    return(xml_text(xml_find_first(xml, fullpath)))
}


## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
## get the basename for outputting the simulation results from a model xml
abm_getoutname <- function(xml){
    return(xml_text(xml_find_first(xml,"///output/basename")))
}

## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
## get the simulation results given an xml model
abm_getsimres_verbose <- function(xml){
    outbasename <- paste0(abm_getoutname(xml))

    globalsname <- paste0(outbasename, ".globals.csv")
    localsname <- paste0(outbasename, ".locals.csv")
    agentsname <- paste0(outbasename, ".agents.csv")

    colnames <- c("x", "y", "t", "type", "name", "value")

    globals <- data.table(type = character(),
                          name = character(),
                          value = numeric())
    locals <- data.table(x = numeric(),
                         y = numeric(),
                         t = numeric(),
                         type = character(),
                         name = character(),
                         value = numeric())
    agents <- data.table(x = numeric(),
                         y = numeric(),
                         t = numeric(),
                         type = character(),
                         name = character(),
                         value = numeric())

    if(!file.exists(globalsname) |
       !file.exists(localsname) |
       !file.exists(agentsname)){
        cat(paste0("one of the files ", globalsname,", ",localsname,", ",
                   agentsname, " does not exist."))

        return(list(globals = globals, locals = locals, agents = agents))
        ## quit(save = "no", status = 1)

    } else{


        tryCatch({
            globals <- as.data.table(read.table(globalsname,
                                                sep = ",", header = TRUE,
                                                fill = TRUE,
                                                stringsAsFactors = FALSE))
            if("value" %in% names(globals)){
                globals[, value := as.numeric(value)]
            }
            ## return(to_ret)
        }, error = function(err){
            print(paste0("Error reading file ",globalsname,": ",err))
        },finally = {
        })

        tryCatch({
            locals <- as.data.table(read.table(localsname,
                                               sep = ",", header = TRUE,
                                               fill = TRUE,
                                               stringsAsFactors = FALSE,
                                               col.names = colnames))
            ## locals[, value := as.numeric(value)]
            locals[, c("x", "y", "t", "value") :=
                         list(as.numeric(x), as.numeric(y), as.numeric(t),
                              as.numeric(value))]
        }, error = function(err){
            print(paste0("Error reading file ",localsname,": ",err))
          },finally = {
        })

        tryCatch({
            agents <- as.data.table(read.table(agentsname,
                                               sep = ",", header = TRUE,
                                               fill = TRUE,
                                               stringsAsFactors = FALSE,
                                               col.names = colnames))
        }, error = function(err){
            print(paste0("Error reading file ",agentsname,": ",err))
          },finally = {
        })
        ## converting value to numeric value here saves a lot of trouble later
        suppressWarnings(
            agents[name == "agenttype",
                   strvalue := as.numeric(as.factor(as.character(value)))])
        ## suppressWarnings(agents[, value := as.numeric(as.character(value))])
        suppressWarnings(agents[, c("x", "y", "t", "value") :=
                         list(as.numeric(x), as.numeric(y), as.numeric(t),
                              as.numeric(value))])

        agents[name == "agenttype", value := strvalue]
        agents[, strvalue := NULL]

        return(list(globals = globals, locals = locals, agents = agents))
    }
}

## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
## simulate an sbml model
abm_simulate <- function(xmlname, executable, parameters){
    if(Sys.info()['sysname'] == "Linux"){
        executable <- paste0("./",executable)
    }
    system(paste(executable, "--xml",xmlname, parameters))
}

## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
## write xml to  a file, wrap in 'cat' to keep linebreaks
abm_write <- function(xml, xmlname){
    cat(as.character(xml),file = xmlname)
    ## cat(saveXML(xml, encoding = "UTF-8", indent = TRUE),file = xmlname)
}

## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
parse_evalstring <- function(evalstring){
    ## parse a string that is to be evaluaated.Check that it only contains
    ## 'src', numbers, 'e', arithmetic operators, additional operators like
    ##   log, sin, etc and functions to sample from random distributions

    to.ret <- 0
    allowed.strings <- c("src", "\\d+\\.*\\d*", "[+*/-^\\(\\),]", " ", "%%", "%/%",
                         "abs", "sqrt", "ceiling", "floor", "trunc",
                         "cos", "sin", "tan", "acos", "asin", "atan", "atan2",
                         "cospi", "sinpi", "tanpi",
                         "log", "log10", "log2", "exp",
                         "rbeta", "rbinom", "rcauchy", "rchisq", "rexp", "rf",
                         "rgamma", "rgeom", "rhyper", "rlnorm", "rmultinom",
                         "rnbinom", "rnorm", "rpois", "rt","runif", "rweibull",
                         "beta", "lbeta", "gamma", "lgamma", "psigamma", "digamme",
                         "trigamma", "choose", "lchoose","factorial", "lfactorial",
                         ls())
    leftover <- evalstring
    for(elem in allowed.strings){

        leftover <- gsub(elem, "", leftover)
    }
    if(nchar(leftover) > 0){

        to.ret <- 1
        .Last <- function() {
            cat("Error: illegal characters in string to be evaluated!\n")
            cat("Please check your input...\n")
        }
        quit("no")
    }
    ## could check if there are words like 'unlink' and 'system inside'
return(to.ret)
}


## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
get_simdata <- function(folders, par1.names, par1.paths, par1.factor, par1.num, par1.log, reps, exprs){
    ## load sets of simulation data from sensitivity analysis and
    ## compute a value to plot later
    ## load data
    results <- data.table(parval = numeric(),
                          meanval = numeric(),
                          sdval = numeric(),
                          parname = character(),
                          parindex = numeric(),
                          relparval = numeric())

    for(j in c(1:length(folders))){
        cat(paste0("..", folders[j],"\n"))

        setwd(folders[j])
        WT.content <- abm_readxml(WTname)

        seq.line <- abm_get_entity_at_path(
            WT.content, par1.paths[j], par1.names[j])
        seq.center <- NA
        if(grepl(",", seq.line)){
            new.val <- strsplit(seq.line, ",")[[1]]
            seq.center <- as.numeric(new.val[2])
        } else{
            seq.center <- as.numeric(seq.line)
        }

        par1.seq <- lseq(seq.center * (1 - par1.factor),
                         seq.center * (1 + par1.factor),
                         length.out = par1.num, logit = par1.log)
        par1.seq <- unique(c(par1.seq, seq.center))

        outname.original.WT <- abm_getoutname(WT.content)

        for(i in 1:length(par1.seq)){
            cat(paste0("\t",i,"\n"))
            res.col <- c()
            for (k in 1:reps){

                ## load simresults into datatables
                WTname.new <- gsub(".xml", paste0("_", par1.names[j], "_",
                                                  i, "_", k, ".xml"),
                                   WTname)

                simres.WT <- abm_getsimres_verbose(abm_readxml(WTname.new))

                idim <- max(simres.WT$agents$x)
                jdim <- max(simres.WT$agents$y)


                ## compute results
                ov <- eval(parse(text = exprs))
                res.col <- c(res.col, ov)
            }
            results <- rbind(results,
                             list(par1.seq[i],
                                  mean(res.col), sd(res.col),
                                  par1.names[j], i,
                                  par1.seq[i] / seq.center
                                  ))
        }
    }
    return(results)
}


## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
## plot a given variable for a given list of agentIDs over time
## assumes concise output as prepared in reformar_verbose_output
plot_agent_var <- function(agent.dt, var.name, t.name, ID.name,
                           agentIDs, show = FALSE, file.name = "", log = FALSE){
    if(var.name %in% names(agent.dt)){
        ## prepare data
        agent.curr <- agent.dt[get(ID.name) %in% agentIDs]
        set(agent.curr, j=ID.name, value=as.factor(agent.curr[[ID.name]]))

        ## generate plot
        testplot <- ggplot(data = agent.curr,
                           aes_(x = as.name(t.name),
                                y = as.name(var.name),
                                color = as.name(ID.name)))
        testplot <- testplot + geom_line(size = 2)
        testplot <- testplot + geom_point(size = 4)
        testplot <- testplot + theme_bw()
        if(log){
            testplot <- testplot +scale_y_continuous(trans = 'log10')
        }

        ## output plot
        if(show){show(testplot)}
        if(nchar(file.name) > 0){
            ggsave(filename = file.name, plot = testplot)
            }
    }
}

## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
## Here ome functions for working with compact output mainly, because it just
## saves a lot of memeory

## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
## convert verbose output to concise output:
## reshape: put all agents' variables in columns
## agent.dt should be attained from abm_getsimres_verbose
reformat_verbose_output <- function(simres, save = FALSE,
                                    filename = "reformatted.R",
                                    s.compress = FALSE,
                                    s.compression_level = 0){

    ## reformat locals
    locals.unique <- data.table()
    if(dim(simres$locals)[1]>0){
        ## locals.dt <- simres$locals
        locals.unique <- dcast(simres$locals, x + y + t ~ name)
    }

    ## reformat agents
    agents.unique <- data.table()
    if(dim(simres$agents)[1]>0){
        ## agents.dt <- simres$agents
        agents.unique <- dcast(simres$agents, x + y + t ~ name)
    }

    return(list(globals = simres$globals, locals = locals.unique,
                agents = agents.unique))
}

##

## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
## read compact out
abm_getsimres_compact <- function(xml){
    outbasename <- paste0(abm_getoutname(xml))

    globalsname <- paste0(outbasename, ".globals.csv")
    localsname <- paste0(outbasename, ".locals.csv")
    agentsname <- paste0(outbasename, ".agents.csv")

    globals <- data.table(type = character(),
                          name = character(),
                          value = numeric())
    locals <- data.table()
    agents <- data.table()

    if(!file.exists(globalsname) |
       !file.exists(localsname) |
       !file.exists(agentsname)){
        cat(paste0("one of the files ", globalsname,", ",localsname,", ",
                   agentsname, " does not exist."))

        return(list(globals = globals, locals = locals, agents = agents))

    } else{

        tryCatch({
            globals <- as.data.table(read.table(globalsname,
                                                sep = ",", header = TRUE,
                                                fill = TRUE,
                                                stringsAsFactors = FALSE))
        }, error = function(err){
            print(paste0("Error reading file ",globalsname,": ",err))
        },finally = {
        })

        tryCatch({
            locals <- as.data.table(read.table(localsname,
                                               sep = ",", header = TRUE,
                                               fill = TRUE,
                                               stringsAsFactors = FALSE))
        }, error = function(err){
            print(paste0("Error reading file ",localsname,": ",err))
          },finally = {
        })

        tryCatch({
            agents <- as.data.table(read.table(agentsname,
                                               sep = ",", header = TRUE,
                                               fill = TRUE,
                                               stringsAsFactors = FALSE))
        }, error = function(err){
            print(paste0("Error reading file ",agentsname,": ",err))
          },finally = {
        })
        ## converting value to numeric value here saves a lot of trouble later
        suppressWarnings(
            agents[,type := as.numeric(as.factor(as.character(type)))])

        return(list(globals = globals, locals = locals, agents = agents))
    }
}

## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
## check datatype of a file and read accordingly
abm_getsimres <- function(xml){

    output.format <- xml_text(xml_find_first(xml,"///output/output_format"))
    if(output.format == "compact"){
        return(abm_getsimres_compact(xml))
    }
    else if(output.format == "full"){
        cat("getting verbose data\n")
        simres.verbose <- abm_getsimres_verbose(xml)
        cat("converting verbose data\n")
        simres.compact <- reformat_verbose_output(simres.verbose)
        return(simres.compact)

    }
    else{
        return(list(globals = globals, locals = locals, agents = agents))
    }
}

## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
## plotting with compact data

## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
## Function to plot cells from 2D field over time:
##     one pdf for each variable containing pages for each time step
##         that contain a 2d plot of the variables values.
plotCells2D_FAST <- function(datatype, data.dt, idim, jdim, outbasename, ffmpegname, varnames, palette, log_agent = FALSE, log_locals = FALSE){
    ColRamps <- list(viridis(1000), magma(1000), plasma(1000), inferno(1000))

    maxdims <- c(max(data.dt$x, na.rm = TRUE), max(data.dt$y, na.rm = TRUE))
    max.t.digits <- nchar(max(data.dt$t))

    ## do the plotting
    for(cname in colnames(data.dt)[4:length(colnames(data.dt))]){
        if(!(cname %in% varnames)){
            next
        }
        cat(paste0("plotting ", cname, ":"))

        ccolramp <- ColRamps[[floor(runif(1,1,length(ColRamps)))]]

        currdirname <- paste0("Plots/",cname)
        if(dir.exists(currdirname)){

            unlink(currdirname, recursive = TRUE)
        }
        dir.create(currdirname, showWarnings = FALSE)
        ## temp <- data.dt[typename == typenames[i]]
        outname <- paste(outbasename, datatype, cname, sep = "_")
        if(datatype == "globals"){

            ## dealing with global vairables that can be plotted as lines
            testplot <- ggplot(data = data.dt, aes_(x = quote(t),
                                                    y = as.name(cname)))
            testplot <- testplot + geom_line(size = 2)
            testplot <- testplot + theme_bw()
            ggsave(testplot, filename = paste0(currdirname, "/",outname, ".png"))
            ggsave(testplot, filename = paste0(currdirname, "/",outname, ".pdf"))

        }else if(datatype == "locals"){
            ## do plotting for localvars
            colornum <- 10
            ## temp[, value := as.numeric(as.character(value))]
            mmvals <- c(min(data.dt[[cname]]), max(data.dt[[cname]]))
            if(mmvals[1] == mmvals[2]){

                mmvals[2] <- mmvals[1] + 1
            }
            for(step in rev(unique(data.dt$t))){

                if(step %% 100 == 0){

                    cat(step)
                } else{

                    cat(".")
                }
                toplot.dt <- data.dt[t == step]

                fstep <- formatC(step, width = max.t.digits, flag = 0)
                png(paste0(currdirname, "/", outname, "_", fstep, ".png"))
                plot_local_T(toplot.dt, mmvals,
                             ccolramp,
                             cname, step, log_locals)
                dev.off()

            }

            ## make movie
            cat(";;;making movie;;;")
            system(paste(ffmpegname,
                         "-framerate 24",
                         "-pattern_type sequence",
                         "-loglevel error",
                         "-y",
                         "-i",
                         paste0(currdirname, "/", outname,"_%0",max.t.digits,"d.png"),
                         paste0(outname,".mp4")))
            ## delete individual png files
            cat(";;;deleting pngs;;;")
            unlink(currdirname, recursive = TRUE)
            cat("\n")

        }else if(datatype == "agents"){
            ## plot agents variables (fill toplot as with agent.agenttype
            ##  but use continunous scale as with localvars)

            colornum <- 10
            mmvals <- c(min(data.dt[[cname]]), max(data.dt[[cname]]))
            if(mmvals[1] == mmvals[2]){

                mmvals[1] <- 0
                if(mmvals[1] == mmvals[2]){

                    mmvals[2] <- 1
                }
            }
            toplot.dt <- subset(data.dt, , c("t", "x", "y", cname))
            setnames(toplot.dt, names(toplot.dt), c("t", "x", "y", "value"))

            colornum <- max(3,min(1000, length(unique(toplot.dt[, value]))))

            for(step in rev(unique(toplot.dt$t))){

                if(step %% 100 == 0){

                    cat(step)
                } else{

                    cat(".")
                }

                toplot.t.dt <- subset(toplot.dt[t == step], , c("x", "y", "value") )
                fstep <- formatC(step, width = max.t.digits, flag = 0)
                png(paste0(currdirname, "/", outname, "_", fstep, ".png"))
                plot_agent_T(toplot.t.dt, idim, jdim, mmvals,
                             ccolramp,
                             cname, step, log_agent, colornum)
                dev.off()
            }

            ## make movie
            cat(";;;making movie;;;")
            system(paste(ffmpegname,
                         "-framerate 24",
                         "-pattern_type sequence",
                         "-loglevel error",
                         "-y",
                         "-i",
                         paste0(currdirname, "/", outname,"_%0",max.t.digits,"d.png"),
                         paste0(outname,".mp4")))

            ## delete individual png files
            cat(";;;deleting pngs;;;")
            unlink(currdirname, recursive = TRUE)
            cat("\n")
        }
    }
}

## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
## Function to plot agents from 2D field at one time point:
plot_agent_T <- function(data.dt, idim, jdim, mmvals, colramp, varname, step,
                         log, colornum){

    totest.dt <- data.table(x = rep(c(0:idim), jdim + 1),
                            y = rep(0:jdim, each = idim + 1),
                            value = rep(NA, (jdim + 1) * (idim + 1)))
    ## totest.dt[, value := as.integer(value)]
    ## colnames(totest.dt) <- c("x","y", varname)

    ## fill totest.dt with toplot.dt
    tojoin.dt <- merge(data.dt,
                       totest.dt, all.y = TRUE,
                       by = c("x", "y"))
    toplot.dt <- subset(tojoin.dt, ,c("x", "y",  "value.x"))
    ## toplot.dt <- subset(data.dt, , c("x", "y", "value"))
    ## toplot.dt <- data.dt
    setnames(toplot.dt, names(toplot.dt), c("i", "j", "value"))
    ## dims.dt <- data.table(x = c(0,idim), y = c(0, jdim), value = c(NA, NA))
    ## toplot.dt <- rbindlist(list(toplot.dt, dims.dt))
    ## toplot.dt[, value := as.numeric(value)]

    ## convert to logarithm
    if(log == TRUE & !(0 %in% mmvals) & !(0 %in% toplot.dt$value)){
        toplot.dt[, value := log(value)]
        mmvals <- log(mmvals)
    }

    ## catch problems from no increasing x and y values in image.plot
    if(min(toplot.dt[!is.na(value), value]) ==
       max(toplot.dt[!is.na(value), value])){
        toplot.dt[i == 0 & j == 0, value := 1]
    }

    ## plotting with base
    ## blackrings <- c(rep('black', times = dim(toplot.dt)[1]-2), NA, NA)
    ## palette(viridis(colornum))
    ## retplot <- plot(x = toplot.dt[, i],
    ##                 y = toplot.dt[, j],
    ##                 pch = 21,
    ##                 bg = toplot.dt[, value],
    ##                 col = blackrings, #toplot.dt[, value],
    ##                 xlab = "i",
    ##                 ylab = "j",
    ##                 main = paste0(varname," @step ", step),
    ##                 cex = 1.5
    ##                 )

    ## image.plot(toplot.t.dt$x,toplot.t.dt$y, toplot.t.dt$value)

    zvals <- t(matrix(toplot.dt$value,
                      nrow = max(toplot.dt$j + 1),
                      ncol = max(toplot.dt$i + 1)))
    retplot <- image.plot(x = c(unique(toplot.dt$i), max(toplot.dt$i) + 1),
                          y = c(unique(toplot.dt$j), max(toplot.dt$j) + 1),
                          z = zvals,
                          col = colramp,## colorRampPalette(brewer.pal(9,ColRamps[i]))(1000),
                          xlab = "i",
                          ylab = "j",
                          main = paste0(varname," @step ", step),
                          zlim = mmvals
                          )##brewer.pal(100, ColRamps[i]))##rainbow(100))
    return(retplot)
}


## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
## Function to plot local variables from 2D field at one time point:
plot_local_T <- function(data.dt, mmvals, colramp, varname, step, log){

    toplot.dt <- subset(data.dt, ,c("x", "y", varname))
    setnames(toplot.dt, names(toplot.dt), c("i", "j", "value"))

    ## convert to logarithm
    if(log == TRUE & !(0 %in% mmvals) & !(0 %in% toplot.dt$value)){
        toplot.dt[, value := log(value)]
        mmvals <- log(mmvals)
    }


    ## plotting with base
    zvals <- t(matrix(toplot.dt$value,
                      nrow = max(toplot.dt$j + 1),
                      ncol = max(toplot.dt$i + 1)))
    retplot <- image.plot(x = c(unique(toplot.dt$i), max(toplot.dt$i) + 1),
                          y = c(unique(toplot.dt$j), max(toplot.dt$j) + 1),
                          z = zvals,
                          col = colramp,## colorRampPalette(brewer.pal(9,ColRamps[i]))(1000),
                          xlab = "i",
                          ylab = "j",
                          main = paste0(varname," @step ", step),
                          zlim = mmvals
                          )##brewer.pal(100, ColRamps[i]))##rainbow(100))
    return(retplot)
}


## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
## Function to count occurences of character in string
## obtained from
## https://techoverflow.net/2012/11/10/r-count-occurrences-of-character-in-string/
countCharOccurrences <- function(char, s) {
    s2 <- gsub(char,"",s)
    return (nchar(s) - nchar(s2))
}

## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
## Function to read in generic simulation results from a file
load_simres <- function(file.name, file.format = NA, file.sep = NA, file.header = NA){
    file.name <- "S:/C09/JWI/MB/CMB/CLEMENS KUEHN/temp/CopasiResults.txt"

    to.ret <- data.table()
    ## if separator and header is given:
    if(!is.na(file.sep) & !is.na(file.header)){
        to.ret <- as.data.table(read.table(file.name, header = file.header,
                                           sep = file.sep))
    } else if(!is.na(file.format)){
        ## AngioABM output
        if(file.format == "AngioABM"){
            tryCatch({
                to.ret <- as.data.table(read.table(file.name,
                                                   sep = ",", header = TRUE,
                                                   fill = TRUE,
                                                   stringsAsFactors = FALSE))
            }, error = function(err){
                print(paste0("Error reading file ",file.name,": ",err))
            },finally = {
            })

        }
        ## Copasi output
        else if(file.format == "Copasi"){
            tryCatch({
                to.ret <- as.data.table(read.table(file.name,
                                                   sep = "\t", header = FALSE,
                                                   fill = TRUE,
                                                   skip = 1,
                                                   stringsAsFactors = FALSE,
                                                   comment.char = ""))
                f <- file(file.name, 'r')
                firstline <- readLines(f, n = 1)
                close(f)

                to.ret[[length(to.ret)]] <- NULL
                names(to.ret) <- strsplit(firstline, "\t")[[1]]
            }, error = function(err){
                print(paste0("Error reading file ",file.name,": ",err))
            },finally = {
            })

        }
    } else{
        tryCatch({
            f <- file(file.name, 'r')
            ## read the first three lines to
            ## check if it looks like a known format and read accordingly:
            threelines <- readLines(f, n = 3)
            twolines <- threelines[-1]
            firstline <- threelines[1]
            close(f)
            ## check for common separators
            cust.sep <- ""
            candidates <- c(",", " ", "\t")
            for(cand in candidates){
                testing <- unique(countCharOccurrences(cand, twolines))
                if(length(testing) == 1 & testing[1] > 0){
                    cust.sep <- cand
                    break
                }
            }

            has.head <- FALSE
            skip.head <- 0
            header <- suppressWarnings(lapply(strsplit(firstline, cust.sep),
                                              as.numeric))
            if(length(header[[1]])==length(strsplit(threelines[2], cust.sep)[[1]])){
                if(NA%in%header[[1]]){
                    has.head <- TRUE
                }
            } else{
                skip.head <- 1
            }

            to.ret <- as.data.table(read.table(file.name,
                                               sep = cust.sep,
                                               header = has.head,
                                               skip = skip.head,
                                               fill = TRUE,
                                               stringsAsFactors = FALSE,
                                               comment.char = ""))
            ## check if last column is NA (due to trailing separator)
            if(is.na(unique(to.ret[[length(to.ret)]]))){
                to.ret[[length(to.ret)]] <- NULL
            }
            ## check header (might have gotten broken w/ trailing separator)
            if(has.head == TRUE){
                if(all(strsplit(firstline, cust.sep)[[1]] != names(to.ret))){
                    names(to.ret) <- strsplit(firstline, cust.sep)[[1]]
                }
            }

        }, error = function(err){
            print(paste0("Error reading file ",file.name,": ",err))
        },finally = {
        })
    }
    ## check for columns t,x,y and insert them is not there
    if(colnames())

    return(to.ret)

}


## ## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
## ## DEPRECATED! needs to be converted to compact output
## ## make nice plots of agents and locals, using viridis color palettes
## plotOverlay_publication <- function(local.dt, agent.dt, idim, jdim, local.bounds, agent.bounds, step,filename){

##     ## prepare agents
##     totest.dt <- data.table(x = rep(c(0:idim), jdim + 1),
##                             y = rep(0:jdim, each = idim + 1),
##                             value = rep(NA, (jdim + 1) * (idim + 1)))

##     tojoin.dt <- merge(agent.dt[t == step], totest.dt, all.y = TRUE,
##                        by = c("x", "y"))
##     tojoin.dt[, value := value.x]
##     tojoin.dt <- tojoin.dt[, .(x,y,value)]

##     zvals.agent <- t(matrix(tojoin.dt[,value],
##                             nrow = max(local.dt$y + 1),
##                             ncol = max(local.dt$x + 1)))

##     ## plotting locals
##     zvals.local <- t(matrix(local.dt[t==step, value],
##                             nrow = max(local.dt$y + 1),
##                             ncol = max(local.dt$x + 1)))
##     image(x = c(unique(local.dt$x), max(local.dt$x) + 1),
##           y = c(unique(local.dt$y), max(local.dt$y) + 1),
##           z = zvals.local,
##           col = viridis_pal(option = 'inferno',
##                             end = 0.7, begin = 0.3)(1000),
##           ## xlab = "x",
##           ## ylab = "y",
##           zlim = local.bounds,
##           axes=FALSE,xlab="",ylab=""
##           )
##     ## plot agents on top
##     n <- ceiling(agents.bounds[2]-agents.bounds[1])
##     print(paste0("n is ",n))
##     palette(viridis_pal()(n))

##     points(x = agent.dt[t==step,x+1],
##            y = agent.dt[t==step,y+1],
##            bg = agent.dt[t==step,value],
##            pch = 21,
##            col = 'black',
##            cex =0.75
##            )
##     axis(side = 2, at = c(0,10,20,30,40,50),
##          labels = if(filename == "VEGFR1_KO_var2.xml") c(0,10,20,30,40,50)
##                   else FALSE)
##     axis(side = 1, at = c(0,10,20,30,40,50),
##          labels = if(step == 199) c(0,10,20,30,40,50) else FALSE)
## }


## ## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
## ## as plotOverlay but uses image.plot to get legends.
## plotLegends_publication <- function(local.dt, agent.dt, idim, jdim,
##                                     local.bounds, agent.bounds, step,
##                                     filename){

##     ## prepare agents
##     totest.dt <- data.table(x = rep(c(0:idim), jdim + 1),
##                             y = rep(0:jdim, each = idim + 1),
##                             value = rep(NA, (jdim + 1) * (idim + 1)))

##     tojoin.dt <- merge(agent.dt[t == step], totest.dt, all.y = TRUE,
##                        by = c("x", "y"))
##     tojoin.dt[, value := value.x]
##     tojoin.dt <- tojoin.dt[, .(x,y,value)]

##     zvals.agent <- t(matrix(tojoin.dt[,value],
##                             nrow = max(local.dt$y + 1),
##                             ncol = max(local.dt$x + 1)))

##     ## plotting locals
##     zvals.local <- t(matrix(local.dt[t==step, value],
##                             nrow = max(local.dt$y + 1),
##                             ncol = max(local.dt$x + 1)))

##     pdf(paste0("leg_local.pdf"), width = 7, height = 4/3*7)

##     plot(c(unique(tojoin.dt$x), max(tojoin.dt$x) + 1),
##          c(unique(tojoin.dt$y), max(tojoin.dt$y) + 1), type = "n")
##     image.plot(x = c(unique(local.dt$x), max(local.dt$x) + 1),
##           y = c(unique(local.dt$y), max(local.dt$y) + 1),
##           z = zvals.local,
##           col = viridis_pal(option = 'inferno', end = 0.7, begin = 0.3)(1000),
##           zlim = local.bounds,
##           add = TRUE,
##           axes=FALSE,xlab="",ylab=""
##           )
##     dev.off()
##     ## plot agents on top

##     n <- ceiling(agents.bounds[2]-agents.bounds[1])
##     pdf(paste0("leg_agents.pdf"), width = 7, height = 4/3*7)
##     plot(c(unique(tojoin.dt$x), max(tojoin.dt$x) + 1),
##          c(unique(tojoin.dt$y), max(tojoin.dt$y) + 1), type = "n")
##     image.plot(x = c(unique(tojoin.dt$x), max(tojoin.dt$x) + 1),
##           y = c(unique(tojoin.dt$y), max(tojoin.dt$y) + 1),
##           z = zvals.agent,
##           col = viridis_pal()(n),
##           xlab = "i",
##           ylab = "j",
##           zlim = c(0,agent.bounds[2]),
##           add = TRUE
##           )
##     dev.off()
## }
