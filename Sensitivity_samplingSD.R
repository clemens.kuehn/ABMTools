#!/usr/bin/Rscript

## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
## compute parameter sensitivity by varying the sd of the distributions from
## which parameters are drawn. Calls simulator for these models.
## Does not compute any analysis or plotting of the results.
##
## Assumes AngioABM for running simulations and geenrating output.
## 
## Author: Clemens Kuehn
## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


## libs
library(ggplot2)
library(data.table)
library(RColorBrewer)

## import Utils.R
source("../ABMTools/Utils.R")

## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
## 0.parse arguments
args <- commandArgs(TRUE)

if (args[1] == "-h" | args[1] == "--h" | length(args) < 5){
    cat("\n\nRun a simulation of an input file varying a given parameter over a sequence of vlaues. Sequence is generated using seq().\n\n")
    cat("Usage:\n\nRscript multifitABM.R xmlfile par1.name par1.start par1.end par1.num_par1.log expr use_mpi \n\n")
    cat("Where arguments are:\n")
    ## 1
    cat(paste0("\t","xmlfile1","\t--", "xml file containing WT model", "\n"))
    ## 2
    cat(paste0("\t","factors","\t--", "factor to change the parameter by", "\n"))
    ## 3
    cat(paste0("\t","reps","\t--", "how many repetitions", "\n"))
    ## 4
    cat(paste0("\t","use_mpi","\t--", "use mpi or not (== 0)", "\n"))
    ## 5
    cat(paste0("\t","sim","\t--", "simulate (== 1) or just compute on exisiting results (== 0)", "\n"))

    quit("no", 11)
}

WTname <- args[1]
par1.factors <- args[2]
reps <- args[3]
use_mpi <- args[4]
simswitch <- args[5]

exec.name <- "Angio_ABM.exe"

WT.content <- abm_readxml(WTname)
outname.original.WT <- abm_getoutname(WT.content)

## 1. split par1.factors into vector
par1.numfactors <- as.numeric(strsplit(par1.factors, ", *")[[1]])
## 2. iterate over each elem of factors
for(i in c(1:length(par1.numfactors))){
    cat(paste0("\n",i))
    for(j in c(1:reps)){
        cat(".")
        WT.new <- WT.content
        parlines <- xml_children(xml_find_all(WT.new, "//agents/agent"))
        ## 2.1 for all parameters whose value starts not with 99: update sd
        for(node in parlines){
            currval <- as.numeric(strsplit(xml_text(node),",")[[1]])
            if(currval[1] == 4 & length(currval) > 1){
                currval[3] <- currval[2] * par1.numfactors[i]
                xml_text(node) <- paste0(currval, collapse = ",")
            }
        }

        ## 2.2 change output name
        outname.new <- paste0(outname.original.WT,"_",i,"_",j)
        abm_modify_path(WT.new, "//model/output", "basename", outname.new)

        ## 2.3 save under different name
        WTname.new <- gsub(".xml",
                           paste0("_", i,"_",j, ".xml"),
                           WTname)
        abm_write(WT.new, WTname.new)

        ## 3.start simulations
        if(simswitch == 1){
            ## simulate (sequential)
            if(use_mpi == 0){
                print(paste0("use_mpi is 0 and filename is ",WTname.new))
                abm_simulate(WTname.new, exec.name, "--verbose 1")
            }else{
                if(Sys.info()['sysname'] == "Linux"){
                    exec.name <- paste0("./",exec.name)
                }
                currname.WT <- WTname.new
                currjobname.WT <- paste("sd", i,j, sep = "_")
                sys.string.WT <- paste0("qsub -m abe -q long",
                                        " -l ncpus=1,nice=19",
                                        " -j oe",
                                        " -o ", currjobname.WT, ".log",                                        
                                        " -N ",currjobname.WT,
                                        " -v directory=",getwd(),
                                        ",execname=",exec.name,
                                        ",xmlname=",currname.WT,
                                        ",jobname=",currjobname.WT,
                                        " PBSscript.pbs")
                ## submit (trying again 10 secs alter in case of failure)
                sys.ret <- system(sys.string.WT, ignore.stdout = TRUE)
                while(sys.ret > 0){
                    Sys.sleep(10)
                    sys.ret <- system(sys.string.WT, ignore.stdout = TRUE)
                }
            }
            
        }

    }
    
}

if(use_mpi == 1 & simswitch == 1){
    cat("waiting for simulations")
    ## check if all are finished (using qstat)
    qstate <- system(paste0("qstat -f | grep ", substr(WTname,1,5)), intern = TRUE)
    while(length(qstate) > 0){
        cat(".")
        Sys.sleep(60)
        qstate <- system(paste0("qstat -f | grep ", substr(WTname,1,5)),
                         intern = TRUE)
    }
}

cat("\n\n\t\tALL SIMULATIONS FINISHED\n\n")


