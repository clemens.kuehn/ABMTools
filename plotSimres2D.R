#!/usr/bin/Rscript

## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
## plot the results of an AngioABM simulation run and turns them into movies
##
## Assumes AngioABM for running simulations and geenrating output.
##
## requires ffmpeg for movie generation.
##
## Author: Clemens Kuehn
## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


## libs
library(ggplot2)
library(data.table)
library(RColorBrewer)

## import Utils.R
source("Utils.R")

## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
## get ffmpeg command ()
ffmpegname <- "ffmpeg"
if(Sys.info()['sysname'] == "Windows"){

    ffmpegname <- "ffmpeg.exe"
    if(suppressWarnings(system(ffmpegname, intern = FALSE,
                               ignore.stderr = TRUE, ignore.stdout = TRUE,
                               show.output.on.console = FALSE)) > 1){

        ffmpegname <- "C:\\Programs\\ffmpeg\\ffmpeg.exe"
        if(suppressWarnings(system(ffmpegname, intern = FALSE,
                                   ignore.stderr=TRUE, ignore.stdout = TRUE,
                                   show.output.on.console = FALSE)) > 1){

            ffmpegname <- paste0("C:\\Users\\", Sys.info()['user'],
                                 "\\_Portable\\ffmpeg\\bin\\ffmpeg.exe")
        }
    }
}
if(suppressWarnings(system(ffmpegname, intern = FALSE,
                           ignore.stderr = TRUE, ignore.stdout = TRUE,
                           show.output.on.console = FALSE)) > 1){

    cat(paste0("Error: can't find ffmpeg executable.\n",
               "You should install ffmpeg and try again.\n",
               "On windows, the executable ffmpeg.exe must be either in\n",
               "\tPATH,\n\tC:\\Programs\\ffmpeg\\bin\\\n",
               "\tC:\\Users\\CURRENTUSER\\_Portable\\ffmpeg\\bin\\\n"))
    q("no", status = 1)
}


## parse parameters (same order as in cpp
args <- commandArgs(TRUE)

if (args[1] == "-h" | args[1] == "--h" | length(args) < 3){
    cat("\n\nRun model simulation for a 2D ABM model, plot the output, make a movie of the plots and delete the plots.\n\tCurrently plots all local variables, all agents separately and then all combinations of local variables and agent variables.\n\n")
    cat("Usage:\n\nRscript plotSimres2D.R execname xmlname outbasename\n\n")
    cat("Where arguments are:\n")
    cat(paste0("\t","execname","\t--", "executable to load model from specification and simulate", "\n"))
    cat(paste0("\t","xmlname","\t\t--", "file with model specification", "\n"))
    cat(paste0("\t","sim","\t--", "simulate (== 1) or just compute on exisiting results (== 0)", "\n"))
    cat(paste0("\t", "varnames", "\t--", "names of variables to plot, separated by ',',nothing or 'all' to plot all variables", "\n"))

    cat("\nFor example input files, see the files contained in the examples folder.\n\n")
    quit("no", 11)
}

execname <- args[1]
xmlname <- args[2]
simswitch <- args[3]
varnames <- ""
if(!is.na(args[4])){
    varnames <- args[4]
}

popup <- args[5]       #should a plot window pop up?

## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
## read the model to get info from it later
le_model <- abm_readxml(xmlname)

## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
## run simulation
if(simswitch > 0){
    abm_simulate(xmlname, execname, "--verbose 1")
}


## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
## load input files and plot individual variables
if(dir.exists("Plots")){

    unlink("Plots", recursive = TRUE)
}
dir.create("Plots", showWarnings = FALSE)

## get simresults
simres <- abm_getsimres(le_model)

## get idim, jdim
idim <- max(simres$locals$x)
jdim <- max(simres$locals$y)

## wtarea <-
##     simres$agents[t==192 & name == 'agentid', .N] / (idim * jdim)

## clustsR <- simres$agents[t == 192 & name == "neighbours" & value >2, .N] / 4 /
##     simres$agents[t == 192 & name == "neighbours", .N]

## tipsT <- simres$agents[t == 192 & name == "tip_hist" & value == 1, .N] /
##     simres$agents[t == 192 & name == "tip_hist", .N]

## reltips <- clustsR / tipsT

## cat(paste0("\n\nStatistics:\n\tWTarea: ", wtarea, "\n\tclusters: ", clustsR,
##            "\n\ttipsT: ", tipsT, "\n\treltips: ", reltips, "\n\n"))

if(nchar(varnames) == 0){
    varnames <- c(colnames(simres$agents)[4:length(colnames(simres$agents))],
                  colnames(simres$locals)[4:length(colnames(simres$agents))])
} else{
    varnames <- strsplit(varnames, ',')[[1]]
}

plotbasename <- paste0("plot_",abm_getoutname(le_model))
if(dim(simres$locals)[1]>0){
    plotCells2D_FAST("locals", simres$locals, idim, jdim, plotbasename, ffmpegname,
                 varnames)
}
if(dim(simres$agents)[1]>0){
    plotCells2D_FAST("agents", simres$agents, idim, jdim, plotbasename, ffmpegname,
                     varnames)
}



## ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
## plot overlays of agent variables and local variables
## plotOverlay(simres$locals, simres$agents, idim, jdim, "", ffmpegname)
